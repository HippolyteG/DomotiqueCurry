<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <link href="css/main.css" rel="stylesheet">
    <title>Auth</title>
  </head>
  <body>
  <div id="container" class= "align-center">
            <!-- zone de connexion -->
        <?php
  session_start();
  if (isset($_SESSION['username'])) {
    if ($_SESSION['username'] !== "") {
        header('Location: video.php');
    }}
    else{
      ?>
            <form action="php/verification.php" method="POST">
                <h1>Domotique Curry</h1></br>  
                <ul>
                  <li>
                    <label class= "champs"><b>Nom d'utilisateur</b></label>
                    <input class="text-margin" type="text" placeholder="Entrer le nom d'utilisateur" name="username" required>
                  </li>
                  <li>
                    <label class= "champs"><b>Mot de passe</b></label>
                    <input class="margin" type="password" placeholder="Entrer le mot de passe" name="password" required>
                  </li>
                  <li></br>
                    <input type="submit" id='submit' value='Login' >
                  </li></br>
                  <li>
                    <input type="button" value="Create account" id="btnDisconnect" onClick="Javascript:window.location.href = 'register.php';"/>
                  </li>
                </ul>
                <?php
                if(isset($_GET['erreur'])){
                    $err = $_GET['erreur'];
                    if($err==1 || $err==2)
                        echo "<p style='color:red'>Utilisateur ou mot de passe incorrect</p>";
                }
              }
                ?>
            </form>
        </div>
  </body>
</html>