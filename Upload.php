<?php
$error = null;
$target_dir = "video/";
$target_file = $target_dir . str_replace(" ", "_", basename($_FILES["fileToUpload"]["name"]));
$uploadOk = 1;
echo $target_file;
$FileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
// Check if file already exists
if (file_exists($target_file)) {
  $error = 1;
  echo "Sorry, file already exists.";
  $uploadOk = 0;
}

// Allow certain file formats
if($FileType != "mp4" && $FileType != "avi" && $FileType != "mov" ) {
  $error = 2;
  echo "Sorry, only MP4, AVI & MOV files are allowed.";
  $uploadOk = 0;
}

// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
  echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
  if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
    $error = 0;
    echo "The file ". htmlspecialchars( basename( $_FILES["fileToUpload"]["name"])). " has been uploaded.";
  } else {
    $error = 4;
    $_SESSION["user_message"] = "Sorry, there was an error uploading your file.";
    echo "Sorry, there was an error uploading your file.";
  }
}


header("Location: http://localhost/DomotiqueCurry/video.php.?message=".$error);
?>