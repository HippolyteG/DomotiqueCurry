<html>

<head>
  <title>Video</title>
  <link rel=stylesheet href="css/video.css">
</head>

<body>
  <?php
  session_start();
  if (isset($_SESSION['username'])) {
    if ($_SESSION['username'] !== "") {
  ?>
    <h1>Your latest video captures :</h1>
    <div>
      <?php
      $files = scandir('video/');
      
        foreach ($files as $filename) {
            $t = explode(".",$filename);
            if (in_array(end($t), array('mp4','avi','mov'))){
              echo '<video width="352" height="200"><source src="video/'.$filename.'" type="video/'.$t[1].'"></video>';
            }
        }
      ?>
    </div>
      <form action="upload.php" method="post" enctype="multipart/form-data">
        Select video to upload:
        <input type="file" name="fileToUpload" id="fileToUpload">
        <input type="submit" value="Upload Video" name="submit">
      </form>
      <footer>
        <input type="button" value="Disconnect" id="btnDisconnect" onClick="Javascript:window.location.href = 'php/disconnect.php';" />
      </footer>
  <?php
    }
  }else{
    ?>
    <h1>Please login or register first</h1>
    <input type="button" value="Login" id="btnLogin" onClick="Javascript:window.location.href = 'index.php';" />
    <input type="button" value="Register" id="btnRegister" onClick="Javascript:window.location.href = 'register.php';" />
  <?php
    }
  if (isset($_GET["message"])){
    if ($_GET["message"] == 1){
      echo "Sorry, file already exists.";
    } elseif($_GET["message"] == 2){
      echo "Sorry, only MP4, AVI & MOV files are allowed.";
    } elseif($_GET["message"] == 4){
      echo "Sorry, there was an error uploading your file.";
    } elseif($_GET["message"] == 0){
      echo "File uploaded !";
    }

  }
  ?>

</body>

</html>