<?php
// Change this to your connection info.
$DATABASE_HOST = 'localhost';
$DATABASE_USER = 'root';
$DATABASE_PASS = '';
$DATABASE_NAME = 'domotiquecurry';
// Try and connect using the info above.
$con = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);
if (mysqli_connect_errno()) {
    // If there is an error with the connection, stop the script and display the error.
    exit('Failed to connect to MySQL: ' . mysqli_connect_error());
}
// Now we check if the data was submitted, isset() function will check if the data exists.
if (!isset($_POST['username'], $_POST['password'])) {
    // Could not get the data that should have been sent.
    header('Location: ../register.php?erreur=5');
}
// Make sure the submitted registration values are not empty.
if (empty($_POST['username']) || empty($_POST['password'])) {
    // One or more values are empty.
    header('Location: ../register.php?erreur=5');
}
// We need to check if the account with that username exists.
if ($stmt = $con->prepare('SELECT password FROM users WHERE username = ?')) {
    // Bind parameters (s = string, i = int, b = blob, etc), hash the password using the PHP password_hash function.
    $stmt->bind_param('s', $_POST['username']);
    $stmt->execute();
    $stmt->store_result();
    // Store the result so we can check if the account exists in the database.
    if ($stmt->num_rows > 0) {
        // Username already exists
        header('Location: ../register.php?erreur=4');
    } else {
        if (preg_match('/^[a-zA-Z0-9]+$/', $_POST['username']) == 0) {
            header('Location: ../register.php?erreur=2');
        }
        if (strlen($_POST['password']) > 20 || strlen($_POST['password']) < 5) {
            header('Location: ../register.php?erreur=3');
        }
        // Username doesnt exists, insert new account
        if ($stmt = $con->prepare('INSERT INTO users (username, password) VALUES (?, ?)')) {
            $password = sha1($_POST['password']);
            $stmt->bind_param('ss', $_POST['username'], $password);
            $stmt->execute();
            header('Location: ../register.php?success=1');
        } else {
            // Something is wrong with the sql statement, check to make sure users table exists with all 3 fields.
            header('Location: ../register.php?erreur=1');
        }
    }
    $stmt->close();
} else {
    // Something is wrong with the sql statement, check to make sure users table exists with all 3 fields.
    header('Location: ../register.php?erreur=1');
}
$con->close();
