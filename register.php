<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<link href="css/main.css" rel="stylesheet">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
		<title>Register</title>
	</head>
	<body>
		<div id="register" class="align-center">
			
			<form action="php/createUser.php" method="post" autocomplete="off">
				<h1>Register</h1>
				<ul class = "marginTop">
                  <li>
					<label for="username">
						<i class="fas fa-user"></i>
					</label>
					<input class="registerText" type="text" name="username" placeholder="Username" id="username" required>
				  </li>
				  <li>
					<label for="password">
						<i class="fas fa-lock"></i>
					</label>
					<input class="registerPassword" type="password" name="password" placeholder="Password" id="password" required>	
				  </li>
				  <li></br>
					<input type="submit" value="Register"/>
				  </li></br>
				  <li>
				  	<input type="button" value="Login" id="btnDisconnect" onClick="Javascript:window.location.href = 'index.php';"/>
				 </li>
				</ul>
                <?php

                if(isset($_GET['erreur']) ){
                    $err = $_GET['erreur'];
                    if($err==1)
                        echo "<p style='color:red'>DATABASE BROKEN</p>";
					if($err==2)
                        echo "<p style='color:red'>Username Invalid/p>";
					if($err==3)
                        echo "<p style='color:red'>Password must be between 5 and 20 characters long!</p>";
					if($err==4)
						echo "<p style='color:red'>Username exists, please choose another</p>";
					if($err==5)
						echo "<p style='color:red'>Please complete the registration form</p>";
				}
				if(isset($_GET['success'])){
					$success = $_GET['success'];
					if($success==1)
						echo "<p style='color:green'>Your account has been created</p>";
					}
                ?>
			</form>
		</div>
	</body>
</html>